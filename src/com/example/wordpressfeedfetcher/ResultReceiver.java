package com.example.wordpressfeedfetcher;

public interface ResultReceiver {
    public void onResult(RestMethod rm);
}
