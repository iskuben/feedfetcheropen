package com.example.wordpressfeedfetcher;

/***
 * Holds comment-info received from JSON-request
 * @author Olof Bjorklund, Kadir Ozgur
 *
 */
public class Comment implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	public String date; 
	public String content;
	public String author;
	
	public Comment() {
		
	}
	
	public void setContent(String comment_content) {
		content = comment_content;
	}

	public String getContent() {
		return content;
	}
	
	public void setDate(String d) {
		date = d;
	}
	
	public String getDate() {
		return date;
	}
	
	public String getAuthor() {
		return author;
	}
	
	public void setAuthor(String a) {
		author = a;
	}
}
