package com.example.wordpressfeedfetcher;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.myfirstapp.R;
import com.example.wordpressfeedfetcher.RestMethod.Type;

/***
 * This activity fetches a JSON-feed from a specific URL, given in RestMethod.
 * The URL, in this case http://innocreate.se/wordpress/?json=1, provides the
 * activity with string in the form of JSON-data which this activity then uses
 * to parse vital information to be displayed in the application.
 * 
 * @author Olof Bjorklund, Kadir Ozgur
 * 
 */
public class DisplayMessageActivity extends Activity implements ResultReceiver {
	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		try {
			setContentView(R.layout.activity_display_message);

			GifMovieView gif = (GifMovieView) findViewById(R.id.loading_gif);
			gif.setMovieResource(R.drawable.ajax_loader);
			gif.setVisibility(View.GONE);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/***
	 * This function fills the layout of the activity with the Posts/Comments
	 * information given in the parameter.
	 * 
	 * @param posts | an ArrayList of Post-objects.
	 */
	private void fillView(ArrayList<Post> posts) {
		/**
		 * This if/else shows/hides the feed-back message given on the screen by
		 * default.
		 */
		TextView feedback = (TextView) findViewById(R.id.feedback);
		if (posts.size() == 0) {
			feedback.setText(R.string.no_posts);
			feedback.setVisibility(View.VISIBLE);
		} else {
			feedback.setVisibility(View.GONE);
		}
		try {
			// The layout is found and all the views within are removed(for the
			// update-process).
			LinearLayout myLayout = (LinearLayout) findViewById(R.id.linearLayout1);
			myLayout.removeAllViews();

			TextView postView, commentView, dateView, PostAuthorView, CommentAuthorView;
			ImageView imageView;

			for (int i = 0; i < posts.size(); i++) {
				// The posts author
				PostAuthorView = new TextView(this);
				PostAuthorView.setTextAppearance(this, R.style.PostAuthorStyle);
				PostAuthorView.setText(posts.get(i).getAuthor());
				myLayout.addView(PostAuthorView);
				// The post date
				dateView = new TextView(this);
				dateView.setTextAppearance(this, R.style.DateStyle);
				dateView.setText(posts.get(i).getDate());
				myLayout.addView(dateView);
				// The post content
				postView = new TextView(this);
				postView.setTextAppearance(this, R.style.PostStyle);
				postView.setText(posts.get(i).getContent());
				myLayout.addView(postView);
				// The post images
				// More comments
				if (posts.get(i).getImage() != null) {
					myLayout.addView(getSeperator());
					imageView = new ImageView(this);
					imageView.setLayoutParams(new LayoutParams(
							LayoutParams.MATCH_PARENT,
							LayoutParams.WRAP_CONTENT));
					imageView.setImageBitmap(posts.get(i).getImage());
					imageView.setAdjustViewBounds(true);
					myLayout.addView(imageView);
					myLayout.addView(getSeperator());
				}

				for (int j = 0; j < posts.get(i).getComments().size(); j++) {
					// Comment author
					CommentAuthorView = new TextView(this);
					CommentAuthorView.setTextAppearance(this,
							R.style.CommentAuthorStyle);
					CommentAuthorView.setText(posts.get(i).getComments().get(j)
							.getAuthor());
					myLayout.addView(CommentAuthorView);
					// Comment date
					dateView = new TextView(this);
					dateView.setTextAppearance(this, R.style.DateStyle);
					dateView.setText(posts.get(i).getComments().get(j)
							.getDate());
					myLayout.addView(dateView);
					// Comment content
					commentView = new TextView(this);
					commentView.setTextAppearance(this, R.style.CommentStyle);
					commentView.setText(posts.get(i).getComments().get(j)
							.getContent());
					myLayout.addView(commentView);
				}
				myLayout.addView(getSeperator());
			}

			GifMovieView gif = (GifMovieView) findViewById(R.id.loading_gif);
			gif.setVisibility(View.GONE);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/***
	 * Returns a freshly constructed separator.
	 * 
	 * @return View
	 */
	private View getSeperator() {
		// This is the line-separator between posts
		View sepView = new View(this);
		sepView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, 1));
		sepView.setBackgroundColor(Color.rgb(51, 51, 51));
		return sepView;
	}

	/***
	 * Called when the user clicks the "Fetch Feed" button. Checks for Internet connection using isOnline function. 
	 * If online, It fires up an asynchronous JSON-request When button is pressed, displays an gif-image
	 * while loading content from JSON.
	 * 
	 * @param view
	 */
	public void fetchJson(View view) {
		TextView feedback = (TextView) findViewById(R.id.feedback);
		if (isOnline()) {
			feedback.setText(R.string.fetching);
			GifMovieView gif = (GifMovieView) findViewById(R.id.loading_gif);
			gif.setVisibility(View.VISIBLE);
			RestMethod fetcher;
			fetcher = new RestMethod();
			fetcher.executeAsync(Type.GET, this);
		} else {
			feedback.setText(R.string.no_connection);
			feedback.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/***
	 * This function parses the data in the JSON-result into Post/Comment
	 * objects. And also calls to update the view accordingly.
	 */
	@Override
	public void onResult(RestMethod rm) {
		ArrayList<Post> a_posts = new ArrayList<Post>(); // Holds post data
		ArrayList<Comment> a_comments;
		if (rm != null && rm.mJSONResponse != null) {
			try {
				JSONArray j_posts = rm.mJSONResponse.getJSONArray("posts");
				JSONArray j_comments;
				JSONObject j_post;
				JSONObject j_comment;
				String post_cue, comment_cue, post_cue_pic;
				Post post;
				Comment comment;
				for (int i = 0; i < j_posts.length(); i++) { // Go through each post
					j_post = (JSONObject) j_posts.get(i);

					a_comments = new ArrayList<Comment>();
					j_comments = j_post.getJSONArray("comments");
					for (int j = 0; j < j_comments.length(); j++) { // Go through each comment
						j_comment = (JSONObject) j_comments.get(j);
						// Create the comment object
						comment = new Comment();
						comment_cue = j_comment.getString("content");
						comment_cue = comment_cue.replaceAll("<[^>]*>", ""); // Remove html-tags
						comment.setContent(comment_cue);
						comment.setDate(j_comment.getString("date"));
						try { // Some comments have no authors
							comment.setAuthor(j_comment.getJSONObject("author")
									.getString("nickname"));
						} catch (JSONException e) {
							comment.setAuthor("Unknown author");
						}
						a_comments.add(comment);
					}
					// Create post object
					post = new Post();
					post_cue = j_post.getString("content"); // Post text
					post_cue_pic = j_post.getString("content"); // Post image
					int srcStart = post_cue_pic.indexOf("src");
					if (srcStart != -1) {
						post_cue_pic = post_cue_pic.substring(srcStart + 5);
						post_cue_pic = post_cue_pic.replaceAll("\\\\", "");
						post_cue_pic = post_cue_pic.substring(0,
								post_cue_pic.indexOf("\"")); // Remove all but the image source URL
						post.setImage(post_cue_pic);
					}
					post_cue = post_cue.replaceAll("<[^>]*>", ""); // Remove html-tags
					post.setContent(post_cue);
					post.setComments(a_comments);
					post.setDate(j_post.getString("date"));
					try { // Some posts have no authors
						post.setAuthor(j_post.getJSONObject("author")
								.getString("nickname"));
					} catch (JSONException e) {
						post.setAuthor("Unknown author");
					}
					a_posts.add(post);
				}
				/**
				 * The updates on the view needs to be done on the UI-thread. A
				 * runnable object is fed to runOnUiThread function (see class
				 * below).
				 */
				runOnUiThread(new UIRunnable(a_posts));

			} catch (JSONException e) {
				e.printStackTrace();
			} catch (Exception ex) {
				ex.printStackTrace();
			}

		}
	}

	private class UIRunnable implements Runnable {
		ArrayList<Post> posts;

		UIRunnable(ArrayList<Post> p) {
			posts = p;
		}

		public void run() {
			fillView(posts);
		}
	}

	/**
	 * Checks for Internet connection. Returns false if there is none, true
	 * otherwise.
	 */
	public boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}
}
