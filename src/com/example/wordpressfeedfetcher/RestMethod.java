package com.example.wordpressfeedfetcher;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;


public class RestMethod {
    public static String BASE_URL = "http://innocreate.se/wordpress/dalarna/?json=1";
    public String mUrl;
    public Type mMethod;
    public ArrayList<String[]> mQueryParameters;
    public String[] mQueryHeader;
    public int mResponseCode;
	public String mMessage;
	public JSONObject mJSONResponse;
	public long mMediaId = -1;
	public Bitmap mBitmapResponse;

    public RestMethod() {
    	
    }
	
	public RestMethod(String baseURL, long id) {
    	this.mMediaId = id;
    	BASE_URL = baseURL;
	}
	public void setUrl(String url) {
        if (!url.startsWith("/")) {
            this.mUrl = "/" + url;
        } else {
            this.mUrl = url;
        }
    }

    public void appendQueryParameter(String name, String value) {
        if (mQueryParameters == null) {
            mQueryParameters = new ArrayList<String[]>();
        }
        String[] keyValuePair = new String[] { name, value };
        mQueryParameters.add(keyValuePair);
    }
    
    public void setHeader(String name, String value) {
        if (mQueryHeader == null) {
        	mQueryHeader = new String[] { name, value };        
        }
    }

    public int execute(Type method) {
        return 200;
    }

    public void executeAsync(Type method, ResultReceiver rec) {
        new RequestTask(method, rec, this).execute();
    }

    private class RequestTask extends AsyncTask<Void, Void, Long> {
        private ResultReceiver mResultReceiver;
        private RestMethod mRestMethod;

        public RequestTask(Type method, ResultReceiver rec, RestMethod rm) {
            mMethod = method;
            this.mResultReceiver = rec;
            this.mRestMethod = rm;
        }

        @Override
        protected Long doInBackground(Void... params) {
            switch (mMethod) {
            case GET:
                String url = BASE_URL;
//                url += mUrl;
                if(mQueryParameters != null) {
	                for (int i = 0; i < mQueryParameters.size(); i++) {
	                    if (i == 0) {
	                        url += "?";
	                    } else {
	                        url += "&";
	                    }
	                    
	                    String paramName = mQueryParameters.get(i)[0];
	                    String paramValue = mQueryParameters.get(i)[1];
	
	                    url += paramName + "=" + paramValue;
	                }
                }
//                if(mMediaId < 0) {
//                	url +=".json";
//                }
                HttpGet request = new HttpGet(url);
                if( mQueryHeader != null) {
                	request.addHeader(mQueryHeader[0], mQueryHeader[1]);
                }
                
                try {
					executeRequest(request);
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
                break;
            case POST:
                break;
            case PUT:
                break;
            default:
                break;
            }
            mResultReceiver.onResult(mRestMethod);
            return null;
        }
        
        @Override
        protected void onPostExecute(Long result) {
        	super.onPostExecute(result);
        	
        }
    }
    // URI = wat ? n�r requesten skapas, http get objekt, �r URL r�tt ? 
    private void executeRequest(HttpUriRequest request) throws ClientProtocolException, IOException {
		HttpClient client = new DefaultHttpClient();

		HttpResponse httpResponse;

		try {
			httpResponse = client.execute(request);
			mResponseCode = httpResponse.getStatusLine().getStatusCode();
			mMessage = httpResponse.getStatusLine().getReasonPhrase();
			Log.d("innoCreate DEBUG","HttpResponse returned with message " + mResponseCode + "and message" + mMessage);
			HttpEntity entity = httpResponse.getEntity();

			if ( entity != null &&  mResponseCode == 200) {
				InputStream instream = entity.getContent();
				if(mMediaId > -1) {
					mBitmapResponse = convertStreamToBitmap(instream);
				} else {
					mJSONResponse = convertStreamToJSON(instream);
				}

				// Closing the input stream will trigger connection release
				instream.close();
			}

		}
		catch ( ClientProtocolException e ) {
			client.getConnectionManager().shutdown();
			throw e;
		}
		catch ( IOException e ) {
			client.getConnectionManager().shutdown();
			throw e;
		}
	}

    private Bitmap convertStreamToBitmap(InputStream instream) {
    	
    	return BitmapFactory.decodeStream(instream);
	}

	private JSONObject convertStreamToJSON(InputStream instream) {
    	BufferedReader reader = new BufferedReader(new InputStreamReader(instream));
		StringBuilder builder = new StringBuilder();
		JSONObject result = null;

		String line = null;
		try {
			while ( (line = reader.readLine()) != null ) {
				builder.append( line + "\n" );
			}
			result = new JSONObject(builder.toString());
		}
		catch ( IOException e ) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return result;
	}

	public enum Type {
        GET, PUT, POST
    };
}
