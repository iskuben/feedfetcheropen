package com.example.wordpressfeedfetcher;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/***
 * Holds post-info received from JSON-request
 * @author Olof Bjorklund, Kadir Ozgur
 *
 */
public class Post implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	public String date;
	public String content;
	public String title;
	public String author; 
	public String imageUrl;
	public List<Comment> comments;
	private Bitmap image;
	
	public Post() {
		
	}
	
	public List<Comment> getComments() {
		return comments;
	}
	
	public void setComments(List<Comment> v) {
		comments = v;
	}
	
	public void setContent(String post_content) {
		content = post_content;
	}

	public String getContent() {
		return content;
	}

	public void setDate(String d) {
		date = d;
	}

	public String getDate() {
		return date;
	}
	
	public String getAuthor() {
		return author;
	}
	
	public void setAuthor(String a) {
		author = a;
	}
	/***
	 * Loads a image from string URL in order to not lock the UI-thread while loading.
	 * @param u
	 */
	public void setImage(String u) {
		imageUrl = u;
		try {
			image = BitmapFactory.decodeStream((InputStream)new URL(imageUrl).getContent());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Bitmap getImage() {
		return image;
	}

}
