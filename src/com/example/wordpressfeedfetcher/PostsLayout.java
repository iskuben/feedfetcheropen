package com.example.wordpressfeedfetcher;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class PostsLayout extends LinearLayout {

	public PostsLayout(Context context) {
		super(context);
	}
	
	public PostsLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
	
}
